import { createRouter, createWebHistory } from 'vue-router';
import Home from '../views/Home.vue';
import LayananKami from '../views/LayananKami.vue';
import TentangKami from '../views/TentangKami.vue';
import Blog from '../views/Blog.vue';
import Mitra from '../views/Mitra.vue';
import Kontak from '../views/Kontak.vue';
import Chat from '../views/chat.vue';
import Kebijakan from '../views/kebijakan.vue';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/layanankami',
      name: 'LayananKami',
      component: LayananKami
    },
    {
      path: '/tentangkami',
      name: 'TentangKami',
      component: TentangKami
    },
    {
      path: '/blog',
      name: 'Blog',
      component: Blog
    },
    {
      path: '/mitra',
      name: 'Mitra',
      component: Mitra
    },
    {
      path: '/kontak',
      name: 'Kontak',
      component: Kontak
    },
    {
      path: '/chat',
      name: 'Chat',
      component: Chat
    },
    {
      path: '/kebijakan',
      name: 'Kebijakan',
      component: Kebijakan
    },
  ]
});

export default router;